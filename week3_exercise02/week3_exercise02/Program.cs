﻿using System;

namespace week3_exercise02
{
	class MainClass
	{
		public static void Main(string[] args)
		{
			var colours = new string[5] { "red", "blue", "orange", "white", "black" };
			foreach (var x in colours)
				Console.WriteLine(x);
			
		}
	}
}
